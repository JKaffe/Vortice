/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#include "comp_shader.hpp"
#include "vv_buffer.hpp"
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vector>
#include <vulkan/vulkan_core.h>
#include <stdexcept>

#include "vortice.hpp"
#include "vv_wrappers.hpp"
#include "vv_device.hpp"
#include "vv_surface.hpp"
#include "vv_pipeline.hpp"
#include "vv_graphics.hpp"

#include "frag_shader.hpp"
#include "vert_shader.hpp"

static void handleKeyCallback(vortice::Demo& demo, int key, int scancode, int action, int mods) {
	if (action != GLFW_PRESS && action != GLFW_REPEAT)
		return;

	switch (key) {
		case GLFW_KEY_UP:
			demo.incModValue();
			break;

		case GLFW_KEY_DOWN:
			demo.decModValue();
			demo.createNextVertexBuffer();

			break;
	}
}

void vortice::Demo::createNextVertexBuffer() {
	m_state.dirty.vertex_buffers = true;

	// Wait for a trasition of next to current.
	if (m_state.vertex_buffers.next != nullptr)
		return;

	m_state.vertex_buffers.next =
		new vvWrappers::VvVertexInputBuffer(*m_device,
											m_vk_physical_device,
											m_queue_family_idx,
											m_state.mod * 2 * 2 * sizeof(float), // mod * 2 vertecies * vec2()
											VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, // The compute shader will srite to this buffer so making it a sorage buffer too.
											VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
}

void vortice::Demo::incModValue() {
	m_state.mod += 1.0f;
	createNextVertexBuffer();
}

void vortice::Demo::decModValue() {
	m_state.mod = std::max(3.0f, m_state.mod - 1.0f);
	createNextVertexBuffer();
}

void vortice::Demo::transitionVertexBuffer() {
	if (m_state.vertex_buffers.next == nullptr)
		std::runtime_error("Trying to transition vertex buffer but next buffer is nullptr.");

	vvWrappers::VvVertexInputBuffer *old_buffer = std::exchange(m_state.vertex_buffers.current, std::exchange(m_state.vertex_buffers.next, nullptr));
	delete old_buffer;

	static VkDescriptorBufferInfo comp_out_desc_buff_info = {
		.offset = 0,
		.range = VK_WHOLE_SIZE,
	};

	static const VkWriteDescriptorSet write_desc_set = {
		.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
		.dstSet = m_vk_desc_set,
		.descriptorCount = 1,
		.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
		.pBufferInfo = &comp_out_desc_buff_info,
	};

	comp_out_desc_buff_info.buffer = m_state.vertex_buffers.current->unwrap();

	vkUpdateDescriptorSets(m_device->unwrap(), 1, &write_desc_set, 0, 0);
};

vortice::Demo::Demo(const VkApplicationInfo *const app_info) {
	m_window = wGlfwWindow::getInstance(app_info->pApplicationName, { 800, 600 });
	m_window->setKeyCallback(this, &handleKeyCallback);

	std::vector<std::string> layers = { vvWrappers::KHRONOS_VALIDATION_LAYER_NAME };
	std::vector<std::string> activable_layers = vvWrappers::VvInstance::supportsLayers(layers);

	if (layers.size() != activable_layers.size())
		throw std::runtime_error("Some of the requested layers are not available.");

	m_instance = new vvWrappers::VvInstance(app_info, activable_layers, m_window->getRequiredExtensions());

	m_vk_surface = m_window->acquireSurface(m_instance->unwrap());
	m_surface = new vvWrappers::VvSurface(m_vk_surface, *m_instance);

	std::vector<std::string> device_extensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

	std::vector<VkPhysicalDevice> phys_devices = vvWrappers::VvPhysicalDevice::getDevices(*m_instance);
	phys_devices.shrink_to_fit();

	vvWrappers::VvPhysicalDevice::printNames(phys_devices);

	std::optional<uint32_t> queue_family_idx;
	for (auto phys_dev: phys_devices) {
		auto opt_idx = vvWrappers::VvPhysicalDevice::getQueueFamilyIdx(phys_dev, VK_QUEUE_COMPUTE_BIT | VK_QUEUE_GRAPHICS_BIT);
		if (opt_idx.has_value()) {
			if (!vvWrappers::VvPhysicalDevice::hasExtensions(phys_dev, device_extensions))
				continue;

			if (!vvWrappers::VvPhysicalDevice::supportsPresentation(phys_dev, opt_idx.value(), m_surface->unwrap()))
				continue;

			if (!m_surface->supportsFormat(phys_dev, VK_FORMAT_B8G8R8A8_SRGB, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR))
				continue;

			m_vk_physical_device = phys_dev;
			queue_family_idx = opt_idx.value();
		}
	}

	if (!queue_family_idx.has_value())
		throw std::runtime_error("Could not find a suitable queue family.");

	m_queue_family_idx = queue_family_idx.value();

	m_device = new vvWrappers::VvDevice(m_vk_physical_device, m_queue_family_idx, device_extensions);

	// Vulkan doesn't actually do this binding. The wrappers do it just to make things easier.
	m_surface->bindPhysicalDevice(m_vk_physical_device, VK_FORMAT_B8G8R8A8_SRGB, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR);
	m_surface->setSize(m_window->getSize());

	m_swapchain = new vvWrappers::VvSwapchain(*m_device, *m_surface);

	vvWrappers::VvShaderModule vert_module(m_device->unwrap(), VK_SHADER_STAGE_VERTEX_BIT, shader::vertex_spv, (uint32_t)sizeof(shader::vertex_spv));
	vvWrappers::VvShaderModule frag_module(m_device->unwrap(), VK_SHADER_STAGE_FRAGMENT_BIT, shader::fragment_spv, (uint32_t)sizeof(shader::fragment_spv));
	vvWrappers::VvShaderModule comp_module(m_device->unwrap(), VK_SHADER_STAGE_COMPUTE_BIT, shader::compute_spv, (uint32_t)sizeof(shader::compute_spv));

	const VkDescriptorSetLayoutBinding storage_buffer_binding = {
		.binding = 0,
		.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
		.descriptorCount = 1,
		.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
	};

	const VkDescriptorSetLayoutCreateInfo desc_set_layout_create_info = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
		.bindingCount = 1,
		.pBindings = &storage_buffer_binding,
	};

	VkResult result = vkCreateDescriptorSetLayout(m_device->unwrap(), &desc_set_layout_create_info, nullptr, &m_vk_desc_set_layout);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not create descriptor set layout.");

	m_pipeline_layout = new vvWrappers::VvPipelineLayout(m_device->unwrap(), nullptr, &m_vk_desc_set_layout);
	m_render_pass = new vvWrappers::VvRenderPass(*m_surface, *m_device);

	m_state.mod = 8.0f;
	createNextVertexBuffer();

	// This has to match the shader.
	const VkVertexInputBindingDescription vertex_binding_desc = {
		.binding = 0,
		.stride = 2 * sizeof(float),
		.inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
	};
	const VkVertexInputAttributeDescription vertex_attribute_desc = {
		.location = 0,
		.binding = 0,
		.format = VK_FORMAT_R32G32_SFLOAT,
		.offset = 0
	};

	m_graphics_pipeline = new vvWrappers::VvGraphicsPipeline(*m_device,
															 vert_module,
															 frag_module,
															 *m_surface,
															 *m_pipeline_layout,
															 *m_render_pass,
															 &vertex_binding_desc,
															 &vertex_attribute_desc);

	VkComputePipelineCreateInfo comp_pipeline_create_info = {
		.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
		.stage = {
			.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
			.stage = VK_SHADER_STAGE_COMPUTE_BIT,
			.module = comp_module.unwrap(),
			.pName = "main",
		},
		.layout = m_pipeline_layout->unwrap(),
	};

	result = vkCreateComputePipelines(m_device->unwrap(), VK_NULL_HANDLE, 1, &comp_pipeline_create_info, nullptr, &m_vk_compute_pipeline);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not create compute pipeline.");

    const VkDescriptorPoolSize desc_pool_size = {
		.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
		.descriptorCount = 1
    };

    const VkDescriptorPoolCreateInfo desc_pool_create_info = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
		.maxSets = 1,
		.poolSizeCount = 1,
		.pPoolSizes = &desc_pool_size,
    };

    result = vkCreateDescriptorPool(m_device->unwrap(), &desc_pool_create_info, 0, &m_vk_desc_pool);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not create descriptor pool.");

	const VkDescriptorSetAllocateInfo desc_set_alloc_info = {
    	.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
    	.descriptorPool = m_vk_desc_pool,
		.descriptorSetCount = 1,
		.pSetLayouts = &m_vk_desc_set_layout,
    };

    result = vkAllocateDescriptorSets(m_device->unwrap(), &desc_set_alloc_info, &m_vk_desc_set);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not allocate descripto set.");

	m_command_buffer = new vvWrappers::VvCommandBuffer(*m_device, m_queue_family_idx);

	const std::vector<vvWrappers::VvImageView *>& image_views = m_swapchain->getImageViews();
	for (auto img_view_ptr : image_views)
		m_framebuffers.push_back(new vvWrappers::VvFramebuffer(*m_device, *m_render_pass, *img_view_ptr, m_window->getSize()));
}

static void createSync(VkDevice device, VkSemaphore *img_aquired_sem_out, VkSemaphore *ready_to_present_sem_out, VkFence *frame_rendered_fence_out) {
	const VkSemaphoreCreateInfo semaphore_create_info = { .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO };
	VkResult result = vkCreateSemaphore(device, &semaphore_create_info, nullptr, img_aquired_sem_out);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not create img aquire semaphore.");

	result = vkCreateSemaphore(device, &semaphore_create_info, nullptr, ready_to_present_sem_out);
	if (result != VK_SUCCESS) {
		vkDestroySemaphore(device, *img_aquired_sem_out, nullptr);

		throw std::runtime_error("Could not create ready to present semaphore.");
	}

	const VkFenceCreateInfo fence_create_info = {
		.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
		// Needs to be signalled so that we don't wait forever for the first image.
		.flags = VK_FENCE_CREATE_SIGNALED_BIT
	};
	result = vkCreateFence(device, &fence_create_info, nullptr, frame_rendered_fence_out);
	if (result != VK_SUCCESS) {
		vkDestroySemaphore(device, *ready_to_present_sem_out, nullptr);
		vkDestroySemaphore(device, *img_aquired_sem_out, nullptr);

		throw std::runtime_error("Could not create ready to present semaphore.");
	}
}

static inline void destroySync(VkDevice device, VkSemaphore img_aquired_sem, VkSemaphore ready_to_present_sem, VkFence frame_rendered_fence) {
	vkDestroyFence(device, frame_rendered_fence, nullptr);
	vkDestroySemaphore(device, ready_to_present_sem, nullptr);
	vkDestroySemaphore(device, img_aquired_sem, nullptr);
}

static inline void waitForFrameCompletion(VkDevice device, VkFence completion_fence) {
	vkWaitForFences(device, 1, &completion_fence, VK_TRUE, UINT64_MAX);
	vkResetFences(device, 1, &completion_fence);
}

static void presentFrame(uint32_t img_index, VkSemaphore ready_to_present_sem, VkSwapchainKHR swapchain, VkQueue queue) {
	static VkPresentInfoKHR present_info = {
		.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
		.waitSemaphoreCount = 1,
		.pWaitSemaphores = &ready_to_present_sem,
		.swapchainCount = 1,
		.pSwapchains = &swapchain,
	};

	present_info.pImageIndices = &img_index,

	vkQueuePresentKHR(queue, &present_info);
}

void vortice::Demo::mainLoop() {
	VkSemaphore img_aquired_sem, ready_to_present_sem;
	VkFence frame_rendered_fence;

	createSync(m_device->unwrap(), &img_aquired_sem, &ready_to_present_sem, &frame_rendered_fence);

	m_command_buffer->beginRecording();

	const VkPipelineStageFlags wait_dst_stages = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
	const VkCommandBuffer cmd_buffer = m_command_buffer->unwrap();
	const VkSubmitInfo submit_info = {
		.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
		.waitSemaphoreCount = 1,
		.pWaitSemaphores = &img_aquired_sem,
		.pWaitDstStageMask = &wait_dst_stages,
		.commandBufferCount = 1,
		.pCommandBuffers = &cmd_buffer,
		.signalSemaphoreCount = 1,
		.pSignalSemaphores = &ready_to_present_sem,
	};

    while (!m_window->shouldClose()) {
        m_window->pollEvents();

		waitForFrameCompletion(m_device->unwrap(), frame_rendered_fence);

		uint32_t img_idx;
		vkAcquireNextImageKHR(m_device->unwrap(), m_swapchain->unwrap(), UINT64_MAX, img_aquired_sem, VK_NULL_HANDLE, &img_idx);

		m_command_buffer->reset();

		m_command_buffer->beginRecording();
		m_command_buffer->bindPipeline(*m_graphics_pipeline);
		vkCmdBindPipeline(m_command_buffer->unwrap(), VK_PIPELINE_BIND_POINT_COMPUTE, m_vk_compute_pipeline);

		if (m_state.dirty.vertex_buffers) {
			transitionVertexBuffer();

			m_state.dirty.vertex_buffers = false;
		}

		VkBuffer vertex_buffer = m_state.vertex_buffers.current->unwrap();
		VkDeviceSize vertex_buffer_offset = 0;
		vkCmdBindVertexBuffers(m_command_buffer->unwrap(), 0, 1, &vertex_buffer, &vertex_buffer_offset);

		vkCmdBindDescriptorSets(m_command_buffer->unwrap(), VK_PIPELINE_BIND_POINT_COMPUTE, m_pipeline_layout->unwrap(), 0, 1, &m_vk_desc_set, 0, nullptr);
		vkCmdBindDescriptorSets(m_command_buffer->unwrap(), VK_PIPELINE_BIND_POINT_GRAPHICS, m_pipeline_layout->unwrap(), 0, 1, &m_vk_desc_set, 0, nullptr);

		const VkBufferMemoryBarrier buff_barrier = {
			.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
			.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT,
			.dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
			.srcQueueFamilyIndex = 1,
			.dstQueueFamilyIndex = 1,
			.buffer = m_state.vertex_buffers.current->unwrap(),
			.size = VK_WHOLE_SIZE,
		};

		vkCmdPipelineBarrier(m_command_buffer->unwrap(),
							 VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
							 VK_PIPELINE_STAGE_2_VERTEX_INPUT_BIT,
							 0,
							 0,
							 nullptr,
							 1,
							 &buff_barrier,
							 0,
							 nullptr);

		vkCmdDispatch(m_command_buffer->unwrap(), m_state.mod, 1, 1);

		m_command_buffer->beginRenderPass(*m_framebuffers[img_idx], m_window->getSize());
		m_command_buffer->draw((uint32_t) m_state.mod * 2);
		m_command_buffer->endRenderPass();

		m_command_buffer->endRecording();

		VkResult result = vkQueueSubmit(m_device->getQueue(), 1, &submit_info, frame_rendered_fence);
		if (result != VK_SUCCESS)
			throw std::runtime_error("Could not submit work to queue.");

		presentFrame(img_idx, ready_to_present_sem, m_swapchain->unwrap(), m_device->getQueue());
    }

	vkDeviceWaitIdle(m_device->unwrap());

	destroySync(m_device->unwrap(), img_aquired_sem, ready_to_present_sem, frame_rendered_fence);
}

vortice::Demo::~Demo() {
	for (auto framebuffer_ptr : m_framebuffers)
		delete framebuffer_ptr;

	delete m_command_buffer;

	vkFreeDescriptorSets(m_device->unwrap(), m_vk_desc_pool, 1, &m_vk_desc_set);
	vkDestroyDescriptorPool(m_device->unwrap(), m_vk_desc_pool, nullptr);

	vkDestroyPipeline(m_device->unwrap(), m_vk_compute_pipeline, nullptr);
	delete m_graphics_pipeline;

	delete m_state.vertex_buffers.current;
	delete m_state.vertex_buffers.next;

	delete m_render_pass;

	delete m_pipeline_layout;
	vkDestroyDescriptorSetLayout(m_device->unwrap(), m_vk_desc_set_layout, nullptr);

	delete m_swapchain;
	delete m_device;

	delete m_surface;
	m_window->releaseSurface(m_instance->unwrap());

	delete m_instance;
	delete m_window;
}