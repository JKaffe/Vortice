/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdexcept>
#include <vulkan/vulkan_core.h>

#include "vv_surface.hpp"

/*****************************************************************************
	VvSurface.
******************************************************************************/

vvWrappers::VvSurface::VvSurface(VkSurfaceKHR surface, VvInstance& instance) noexcept : instance(instance) {
	m_vk_type = surface;
}

VkSurfaceCapabilitiesKHR vvWrappers::VvSurface::getDeviceCapabilities(VkPhysicalDevice phys_dev) {
	VkSurfaceCapabilitiesKHR capabilities;
	VkResult result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(phys_dev, unwrap(), &capabilities);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not get surface capabilities.");

	return capabilities;
}

std::vector<VkSurfaceFormatKHR> vvWrappers::VvSurface::getDeviceFormats(VkPhysicalDevice phys_dev) {
	uint32_t format_count;
	VkResult result = vkGetPhysicalDeviceSurfaceFormatsKHR(phys_dev, unwrap(), &format_count, nullptr);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not get surface formats.");

	std::vector<VkSurfaceFormatKHR> formats(format_count);
	result = vkGetPhysicalDeviceSurfaceFormatsKHR(phys_dev, unwrap(), &format_count, formats.data());
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not get surface formats.");

	return formats;
}

bool vvWrappers::VvSurface::supportsFormat(VkPhysicalDevice phys_dev, VkFormat format, VkColorSpaceKHR color_space) {
	std::vector<VkSurfaceFormatKHR> formats = getDeviceFormats(phys_dev);
	for (auto& format_info : formats)
		if (format_info.format == format && format_info.colorSpace == color_space)
			return true;

	return false;
}

void vvWrappers::VvSurface::bindPhysicalDevice(VkPhysicalDevice phys_dev, VkFormat format, VkColorSpaceKHR color_space) {
	if (m_phys_dev_bound)
		throw std::runtime_error("Surface is already bound to a physical device.");

	m_phys_dev_bound = true;

	#ifndef NDEBUG
	if (!supportsFormat(phys_dev, format, color_space))
		throw std::runtime_error("Unsupported format or color space.");
	#endif

	m_format = format;
	m_color_space = color_space;

	const VkSurfaceCapabilitiesKHR capabilities = getDeviceCapabilities(phys_dev);

	m_max_size = capabilities.maxImageExtent;
	m_min_size = capabilities.minImageExtent;

	m_size = capabilities.currentExtent;
	// UINT32_MAX is a special vulkan value here.
	if (m_size.height != UINT32_MAX || m_size.width != UINT32_MAX)
		m_size_set = true;

	m_img_count = capabilities.minImageCount + 1;
	if (capabilities.minImageCount == capabilities.maxImageCount)
		m_img_count = capabilities.minImageCount;

	m_transform = capabilities.currentTransform;
}

void vvWrappers::VvSurface::setSize(VkExtent2D size) {
	if (!m_phys_dev_bound)
		throw std::runtime_error("Surface has no bound physical device.");

	size.height = std::clamp(size.height, m_min_size.height, m_max_size.height);
	size.width = std::clamp(size.width, m_min_size.width, m_max_size.width);

	m_size = size;
	m_size_set = true;
}

VkExtent2D vvWrappers::VvSurface::getSize() const {
	if (!m_phys_dev_bound)
		throw std::runtime_error("Surface has no bound physical device.");

	if (!m_size_set)
		throw std::runtime_error("No size was set.");

	return m_size;
}

uint32_t vvWrappers::VvSurface::getImageCount() const {
	if (!m_phys_dev_bound)
		throw std::runtime_error("Surface has no bound physical device.");

	return m_img_count;
}

VkSurfaceTransformFlagBitsKHR vvWrappers::VvSurface::getTransform() const {
	if (!m_phys_dev_bound)
		throw std::runtime_error("Surface has no bound physical device.");

	return m_transform;
}

VkFormat vvWrappers::VvSurface::getFormat() const {
	if (!m_phys_dev_bound)
		throw std::runtime_error("Surface has no bound physical device.");

	return m_format;
}

VkColorSpaceKHR vvWrappers::VvSurface::getColorSpace() const {
	if (!m_phys_dev_bound)
		throw std::runtime_error("Surface has no bound physical device.");

	return m_color_space;
}

/*****************************************************************************
	VvImageView.
******************************************************************************/

vvWrappers::VvImageView::VvImageView(VkDevice device, VkImage image, VkImageViewType type, VkFormat format) : m_vk_device(device){
	const VkImageViewCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
		.image = image,
		.viewType = type,
		.format = format,
		.components = {
			.r = VK_COMPONENT_SWIZZLE_IDENTITY,
			.g = VK_COMPONENT_SWIZZLE_IDENTITY,
			.b = VK_COMPONENT_SWIZZLE_IDENTITY,
			.a = VK_COMPONENT_SWIZZLE_IDENTITY,
		},
		.subresourceRange = {
			.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
			.baseMipLevel = 0,
			.levelCount = 1,
			.baseArrayLayer = 0,
			.layerCount = 1,
		},
	};

	VkResult result = vkCreateImageView(m_vk_device, &create_info, nullptr, &m_vk_type);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not create image view.");
}

vvWrappers::VvImageView::~VvImageView() {
	vkDestroyImageView(m_vk_device, m_vk_type, nullptr);
}