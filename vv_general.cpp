/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#include "vv_wrappers.hpp"

std::vector<std::string> vvWrappers::VvGeneral::pack(const char **cptr, size_t count) noexcept {
	std::vector<std::string> str_vec;

	for (size_t i = 0U; i < count; i++)
		str_vec.push_back(cptr[i]);

	return str_vec;
}

std::vector<const char *> vvWrappers::VvGeneral::unpack(const std::vector<std::string>& str_vec) noexcept {
	std::vector<const char *> cptr_vec;

	for (auto& str : str_vec)
		cptr_vec.push_back(str.c_str());

	return cptr_vec;
}