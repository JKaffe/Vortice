/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "vv_buffer.hpp"
#include <optional>
#include <string>
#include <vector>
#include <vulkan/vulkan_core.h>

struct VkApplicationInfo;

namespace vvWrappers {
    class VvSurface;
    class VvInstance;
    class VvDevice;
    class VvSwapchain;
    class VvPipelineLayout;
    class VvRenderPass;
    class VvGraphicsPipeline;
    class VvCommandBuffer;
    class VvFramebuffer;
}

namespace vortice {
    class Demo;

	class wGlfwWindow {

    public:
        typedef void (*key_callback_fun)(Demo& demo, int key, int scancode, int action, int mods);

    private:
		static wGlfwWindow *instance;

        struct GLFWwindow *m_window;
        std::optional<VkSurfaceKHR> m_vk_surface;

        struct key_callback_state {
            Demo *demo;
            key_callback_fun callback_fun;
        } m_key_callback_state;

	private:
		wGlfwWindow(const char *const title, const VkExtent2D window_size);

	public:
		static wGlfwWindow *getInstance(const char *const title, const VkExtent2D window_size);
		~wGlfwWindow();

		void pollEvents();
		bool shouldClose();

        void setKeyCallback(Demo *demo, key_callback_fun callback);

        VkSurfaceKHR acquireSurface(VkInstance instance);
        void releaseSurface(VkInstance instance);

		VkExtent2D getSize() noexcept;
		std::vector<std::string> getRequiredExtensions();
	};

    class Demo {
    private:
        struct {
            struct {
                vvWrappers::VvVertexInputBuffer *current = nullptr;
                vvWrappers::VvVertexInputBuffer *next = nullptr;
            } vertex_buffers;

            float mod;

            struct {
                bool push_consts;
                // Indicates that the current buffer should be changed to next.
                bool vertex_buffers;
            } dirty;
        } m_state;

        uint32_t m_queue_family_idx;

        VkPhysicalDevice m_vk_physical_device;
        VkSurfaceKHR m_vk_surface;
        VkDescriptorSetLayout m_vk_desc_set_layout;
        VkPipeline m_vk_compute_pipeline;
        VkDescriptorPool m_vk_desc_pool;
        VkDescriptorSet m_vk_desc_set;

        vvWrappers::VvInstance *m_instance;
        vvWrappers::VvSurface *m_surface;
        vvWrappers::VvDevice *m_device;
        vvWrappers::VvSwapchain *m_swapchain;
        vvWrappers::VvPipelineLayout *m_pipeline_layout;
        vvWrappers::VvRenderPass *m_render_pass;
        vvWrappers::VvGraphicsPipeline *m_graphics_pipeline;
        vvWrappers::VvCommandBuffer *m_command_buffer;
        std::vector<vvWrappers::VvFramebuffer *> m_framebuffers;

        wGlfwWindow *m_window;

    private:
        void transitionVertexBuffer();

    public:
        Demo(const VkApplicationInfo *const app_info);
        ~Demo();

        void mainLoop();

        void createNextVertexBuffer();
        void incModValue();
        void decModValue();
    };
}