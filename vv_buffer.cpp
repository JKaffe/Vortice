/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#include "vv_device.hpp"
#include "vv_buffer.hpp"
#include <vulkan/vulkan_core.h>

/*****************************************************************************
	VvBuffer.
******************************************************************************/

vvWrappers::VvBuffer::VvBuffer(VvDevice& device,
							   VkPhysicalDevice physical_device,
							   uint32_t queue_family_idx,
							   uint64_t size,
							   VkBufferUsageFlags usage,
							   VkMemoryPropertyFlags extra_mem_flags)
	: m_vk_device(device.unwrap())
{
	const VkBufferCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.size = size,
		.usage = usage,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.queueFamilyIndexCount = 1,
		.pQueueFamilyIndices = &queue_family_idx,
	};

	VkResult result = vkCreateBuffer(m_vk_device, &create_info, nullptr, &m_vk_type);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not create buffer");

	VkMemoryRequirements mem_reqs;
	vkGetBufferMemoryRequirements(m_vk_device, m_vk_type, &mem_reqs);

	try {
		// TODO: Maybe expose this so that we don't have to get the id each time in case we're recreating the same type of buffers (which we will for the this demo).
		uint32_t mem_heap_idx = vvWrappers::VvPhysicalDevice::getMemoyHeapIdx(physical_device, mem_reqs.memoryTypeBits).value();

		m_dev_mem = new VvDeviceMemory(device, mem_heap_idx, mem_reqs.size);
	} catch (std::runtime_error& exception) {
		std::throw_with_nested(std::runtime_error("Could not allocate device memory for buffer."));
	}

	result = vkBindBufferMemory(m_vk_device, m_vk_type, m_dev_mem->unwrap(), 0);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not bind device memory to buffer");
}

vvWrappers::VvBuffer::~VvBuffer() {
	vkDestroyBuffer(m_vk_device, m_vk_type, nullptr);

	delete m_dev_mem;
}

/*****************************************************************************
	VvVertexInputBuffer.
******************************************************************************/

vvWrappers::VvVertexInputBuffer::VvVertexInputBuffer(VvDevice& device,
													 VkPhysicalDevice phys_dev,
													 uint32_t queue_family_idx,
													 uint64_t size,
													 VkBufferUsageFlags extra_usage,
													 VkMemoryPropertyFlags extra_mem_flags)
	: vvWrappers::VvBuffer(device, phys_dev, queue_family_idx, size, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | extra_usage, extra_mem_flags)
{
}

vvWrappers::VvVertexInputBuffer::~VvVertexInputBuffer() {}