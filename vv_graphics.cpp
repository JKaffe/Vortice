/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#include <exception>
#include <stdexcept>
#include <vulkan/vulkan_core.h>

#include "vv_device.hpp"
#include "vv_surface.hpp"
#include "vv_pipeline.hpp"
#include "vv_graphics.hpp"

/*****************************************************************************
	VvSwapchain.
******************************************************************************/

vvWrappers::VvSwapchain::VvSwapchain(VvDevice& device, VvSurface& surface) : m_vk_device(device.unwrap()), m_format(surface.getFormat()){
	const VkSwapchainCreateInfoKHR create_info = {
		.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		.surface = surface.unwrap(),
		.minImageCount = surface.getImageCount(),
		.imageFormat = m_format,
		.imageColorSpace = surface.getColorSpace(),
		.imageExtent = surface.getSize(),
		.imageArrayLayers = 1,
		.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		// Assuming that the same queue can present as well as handle graphcis.
		.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.preTransform = surface.getTransform(),
		.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
		.presentMode = VK_PRESENT_MODE_FIFO_KHR,
		.clipped = VK_TRUE,
		.oldSwapchain = VK_NULL_HANDLE,
	};

	VkResult result = vkCreateSwapchainKHR(m_vk_device, &create_info, nullptr, &m_vk_type);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not create swapchain.");

	uint32_t image_count;
	result = vkGetSwapchainImagesKHR(m_vk_device, m_vk_type, &image_count, nullptr);
	if (result != VK_SUCCESS) {
		vkDestroySwapchainKHR(m_vk_device, m_vk_type, nullptr);
		throw std::runtime_error("Could not get images.");
	}

	m_images.resize(image_count);
	result = vkGetSwapchainImagesKHR(m_vk_device, m_vk_type, &image_count, m_images.data());
	if (result != VK_SUCCESS) {
		vkDestroySwapchainKHR(m_vk_device, m_vk_type, nullptr);
		throw std::runtime_error("Could not get images.");
	}
}

vvWrappers::VvSwapchain::~VvSwapchain() {
	if (!m_image_views.empty())
		for (auto& view : m_image_views)
			delete view;

	vkDestroySwapchainKHR(m_vk_device, m_vk_type, nullptr);
}

const std::vector<vvWrappers::VvImageView *>& vvWrappers::VvSwapchain::getImageViews() {
	if (m_image_views.empty()) {
		for (auto& img : m_images) {
			m_image_views.push_back(new VvImageView(m_vk_device, img, VK_IMAGE_VIEW_TYPE_2D, m_format));
		}
	}

	return m_image_views;
}

/*****************************************************************************
	VvRenderPass.
******************************************************************************/

vvWrappers::VvRenderPass::VvRenderPass(const VvSurface& surface, const VvDevice& device) : m_vk_device(device.unwrap()){
	const VkAttachmentDescription attachment_desc = {
		.format = surface.getFormat(),
		.samples = VK_SAMPLE_COUNT_1_BIT,
		.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
		.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
		.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
		.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
		.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
		.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
	};

	const VkAttachmentReference attachment_ref = {
		.attachment = 0,
		.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
	};

	const VkSubpassDescription subpass_desc = {
		.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
		.colorAttachmentCount = 1,
		.pColorAttachments = &attachment_ref,
	};

	const VkRenderPassCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
		.attachmentCount = 1,
		.pAttachments = &attachment_desc,
		.subpassCount = 1,
		.pSubpasses = &subpass_desc,
	};

	VkResult result = vkCreateRenderPass(m_vk_device, &create_info, nullptr, &m_vk_type);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not create render pass.");
}

vvWrappers::VvRenderPass::~VvRenderPass() {
	vkDestroyRenderPass(m_vk_device, m_vk_type, nullptr);
}

/*****************************************************************************
	VvGraphicsPipeline.
******************************************************************************/

vvWrappers::VvGraphicsPipeline::VvGraphicsPipeline(const VvDevice& device,
												   const VvShaderModule& vert_module,
												   const VvShaderModule& frag_module,
												   const VvSurface& surface,
												   const VvPipelineLayout& pipeline_layout,
												   const VvRenderPass& render_pass,
												   const VkVertexInputBindingDescription *vertex_binding_desc,
												   const VkVertexInputAttributeDescription *vertex_attribute_desc)
	: m_vk_device(device.unwrap())
{
	const VkPipelineShaderStageCreateInfo shader_create_info[] = {
		vert_module.getPipelineCreateInfoSubStruct(),
		frag_module.getPipelineCreateInfoSubStruct()
	};

	const VkPipelineVertexInputStateCreateInfo vertex_input_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
		.vertexBindingDescriptionCount = vertex_binding_desc != nullptr,
		.pVertexBindingDescriptions = vertex_binding_desc,
		.vertexAttributeDescriptionCount = vertex_attribute_desc != nullptr,
		.pVertexAttributeDescriptions = vertex_attribute_desc,
	};

	const VkPipelineInputAssemblyStateCreateInfo input_assembly_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
		.topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST,
	};

	const VkViewport viewport = {
		.x = 0.0f,
		.y = 0.0f,
		.width = (float)surface.getSize().width,
		.height = (float)surface.getSize().height,
		.minDepth = 0.0f,
		.maxDepth = 1.0f
	};

	const VkRect2D scissor = {
		.offset = {0, 0},
		.extent = surface.getSize()
	};

	const VkPipelineViewportStateCreateInfo viewport_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
		.viewportCount = 1,
		.pViewports = &viewport,
		.scissorCount = 1,
		.pScissors = &scissor,
	};

	const VkPipelineRasterizationStateCreateInfo rasterization_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
		.polygonMode = VK_POLYGON_MODE_FILL,
		.cullMode = VK_CULL_MODE_BACK_BIT,
		.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
		.lineWidth = 1.0f,
	};

	const VkPipelineMultisampleStateCreateInfo multisample_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
		.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
		.minSampleShading = 1.0f,
	};

	const VkPipelineColorBlendAttachmentState color_blend_attachment = {
		.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
	};

	const VkPipelineColorBlendStateCreateInfo color_blend_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
		.attachmentCount = 1,
		.pAttachments = &color_blend_attachment,

	};

	const VkGraphicsPipelineCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
		.stageCount = 2,
		.pStages = shader_create_info,
		.pVertexInputState = &vertex_input_create_info,
		.pInputAssemblyState = &input_assembly_create_info,
		.pViewportState = &viewport_create_info,
		.pRasterizationState = &rasterization_create_info,
		.pMultisampleState = &multisample_create_info,
		.pDepthStencilState = nullptr,
		.pColorBlendState = &color_blend_create_info,
		.pDynamicState = nullptr,
		.layout = pipeline_layout.unwrap(),
		.renderPass = render_pass.unwrap(),
		.subpass = 0,
		.basePipelineHandle = VK_NULL_HANDLE,
		.basePipelineIndex = -1,
	};

	VkResult result = vkCreateGraphicsPipelines(m_vk_device, VK_NULL_HANDLE, 1, &create_info, nullptr, &m_vk_type);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not create graphics pipeline.");
}

vvWrappers::VvGraphicsPipeline::~VvGraphicsPipeline() {
	vkDestroyPipeline(m_vk_device, m_vk_type, nullptr);
}

/*****************************************************************************
	VvFramebuffer.
******************************************************************************/

vvWrappers::VvFramebuffer::VvFramebuffer(VvDevice& device, VvRenderPass& render_pass, VvImageView& img_view, VkExtent2D size) : m_vk_device(device.unwrap()), m_vk_render_pass(render_pass.unwrap()) {
	const VkImageView vk_img_view = img_view.unwrap();
	const VkFramebufferCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
		.renderPass = m_vk_render_pass,
		.attachmentCount = 1,
		.pAttachments = &vk_img_view,
		.width = size.width,
		.height = size.height,
		.layers = 1,
	};

	VkResult result = vkCreateFramebuffer(m_vk_device, &create_info, nullptr, &m_vk_type);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not create framebuffer.");
}

vvWrappers::VvFramebuffer::~VvFramebuffer() {
	vkDestroyFramebuffer(m_vk_device, m_vk_type, nullptr);
}

/*****************************************************************************
	VvCommandBuffer.
******************************************************************************/

vvWrappers::VvCommandBuffer::VvCommandBuffer(VvDevice& device, uint32_t queue_family_idx) : m_vk_device(device.unwrap()) {
	const VkCommandPoolCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
		.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
		.queueFamilyIndex = queue_family_idx,
	};

	VkResult result = vkCreateCommandPool(m_vk_device, &create_info, nullptr, &m_vk_cmd_pool);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not create command pool.");

	const VkCommandBufferAllocateInfo allocate_info = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		.commandPool = m_vk_cmd_pool,
		.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
		.commandBufferCount = 1,
	};

	result = vkAllocateCommandBuffers(m_vk_device, &allocate_info, &m_vk_type);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not allocate command buffer.");

	m_state = INIT;
}

vvWrappers::VvCommandBuffer::~VvCommandBuffer() {
	vkFreeCommandBuffers(m_vk_device, m_vk_cmd_pool, 1, &m_vk_type);
	vkDestroyCommandPool(m_vk_device, m_vk_cmd_pool, nullptr);
}

void vvWrappers::VvCommandBuffer::beginRecording() {
	if (m_state != INIT)
		throw std::runtime_error("Command buffer is not in initial state. Cannot start recording.");

	const VkCommandBufferBeginInfo begin_info = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
	};

	VkResult result = vkBeginCommandBuffer(m_vk_type, &begin_info);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not start command buffer.");

	m_state = RECORDING;
}

void vvWrappers::VvCommandBuffer::endRecording() {
	if (m_state != RECORDING)
		throw std::runtime_error("Command buffer is not in recording state. Cannot end recording.");

	VkResult result = vkEndCommandBuffer(m_vk_type);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not end command buffer.");

	m_state = END;
}

void vvWrappers::VvCommandBuffer::beginRenderPass(VvFramebuffer& framebuffer, VkExtent2D size) {
	if (m_state != RECORDING)
		throw std::runtime_error("Command buffer is not in recording state. Cannot start render pass.");

	const VkClearValue clearColor = { .color = { .float32 = {1.0f, 1.0f, 1.0f, 1.0f}}};

	const VkRenderPassBeginInfo begin_info = {
		.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
		.renderPass = framebuffer.getRenderPass(),
		.framebuffer = framebuffer.unwrap(),
		.renderArea = { .offset = { 0, 0 }, .extent = size },
		.clearValueCount = 1,
		.pClearValues = &clearColor,
	};

	vkCmdBeginRenderPass(m_vk_type, &begin_info, VK_SUBPASS_CONTENTS_INLINE);
}

void vvWrappers::VvCommandBuffer::endRenderPass() {
	if (m_state != RECORDING)
		throw std::runtime_error("Command buffer is not in recording state. Cannot end render pass.");

	vkCmdEndRenderPass(m_vk_type);
}

void vvWrappers::VvCommandBuffer::reset() {
	vkResetCommandBuffer(m_vk_type, 0);
	m_state = INIT;
}

void vvWrappers::VvCommandBuffer::bindPipeline(VvGraphicsPipeline& pipeline) {
	if (m_state != RECORDING)
		throw std::runtime_error("Command buffer is not in recording state. Cannot end render pass.");

	vkCmdBindPipeline(m_vk_type, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.unwrap());
}

void vvWrappers::VvCommandBuffer::draw(uint32_t vertex_count) {
	if (m_state != RECORDING)
		throw std::runtime_error("Command buffer is not in recording state. Cannot end render pass.");

	vkCmdDraw(m_vk_type, vertex_count, 1, 0, 0);
}