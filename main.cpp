/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#include "vortice.hpp"
#include <cstdint>
#include <cstdio>
#include <vulkan/vulkan_core.h>

static const VkApplicationInfo app_info = {
	.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
	.pApplicationName = "Vortice",
	.applicationVersion = VK_MAKE_API_VERSION(0, 0, 1, 0),
	.apiVersion = VK_MAKE_API_VERSION(0, 1, 0, 0)
};

int main() {
	printf("%s %d.%d\n", app_info.pApplicationName, VK_API_VERSION_MAJOR(app_info.applicationVersion), VK_API_VERSION_MINOR(app_info.applicationVersion));
	puts("Copyright (C) 2022 Karmjit Mahil.\n"
		 "License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n"
		 "This is free software: you are free to change and redistribute it.\n"
		 "There is NO WARRANTY, to the extent permitted by law.\n");

	vortice::Demo demo(&app_info);

	demo.mainLoop();

	return 0;
}