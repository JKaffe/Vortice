/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#version 450

layout(set = 0, binding = 0) buffer outBuffer {
    vec2 vertex_buffer[];
};

void main() {
    const uint id = gl_GlobalInvocationID.x;

    // Each invocation writes two vertices and the buffer is a vac2() packed array.
    const uint buff_stride = 2;
    uint buff_idx = id * 2;

    const uint val = id + 1;
    uint mod_value = gl_NumWorkGroups.x;
    uint multiplier = 2048;

    const float two_pi = radians(360.0f);
    float theta_0 = mod(float(val), mod_value) / float(mod_value);
    float theta_1 = mod(float((val + 1) * multiplier), mod_value) / float(mod_value);

    vertex_buffer[buff_idx + 0] = vec2(sin(two_pi * theta_0), -cos(two_pi * theta_0));
    vertex_buffer[buff_idx + 1] = vec2(sin(two_pi * theta_1), -cos(two_pi * theta_1));
}