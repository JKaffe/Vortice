/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#version 450

layout(location = 0) out vec4 color_out;
layout(set = 0, binding = 0) buffer inBuffer {
    vec2 vertex_buffer[];
};

void main() {
    uint id = gl_PrimitiveID;
    const uint buff_stride = 2;
    uint buff_idx = id * 2;

    float len = abs(length(vertex_buffer[buff_idx + 1] - vertex_buffer[buff_idx]));

    vec3 strength = (vec3(1.0, 0.0, 0.0) - vec3(0.0, 1.0, 0.0)) * (len - 0.5) * 0.5 + vec3(0.0, 0.0, 1.0);

    color_out = vec4(strength, 1.0);
}