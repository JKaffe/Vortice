/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include <string>
#include <vulkan/vulkan_core.h>

namespace vvWrappers {
	template <class T> class VvWrapper {
	protected:
		T m_vk_type;
	public:
		T unwrap() const noexcept {
			return m_vk_type;
		};
	};

	class VvGeneral {
	public:
		static std::vector<std::string> pack(const char **cptr, size_t count) noexcept;

		static std::vector<const char *> unpack(const std::vector<std::string>& str_vec) noexcept;
		static bool unpack(VkBool32 val) noexcept {
			return val == VK_TRUE;
		}
	};
};