/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <vulkan/vulkan_core.h>

#include "vv_wrappers.hpp"

namespace vvWrappers {
    class VvDevice;
	class VvDeviceMemory;

	class VvBuffer : public VvWrapper<VkBuffer> {
	private:
		VkDevice m_vk_device;

		VvDeviceMemory *m_dev_mem;
	public:
		VvBuffer(VvDevice& device, VkPhysicalDevice phys_dev, uint32_t queue_family_idx, uint64_t size, VkBufferUsageFlags usage, VkMemoryPropertyFlags extra_mem_flags = 0);
		~VvBuffer();
	};

    class VvVertexInputBuffer : public VvBuffer {
    public:
        VvVertexInputBuffer(VvDevice& device,
                            VkPhysicalDevice phys_dev,
                            uint32_t queue_family_idx,
                            uint64_t size,
                            VkBufferUsageFlags extra_usage = 0,
                            VkMemoryPropertyFlags extra_mem_flags = 0);
        ~VvVertexInputBuffer();
    };
}