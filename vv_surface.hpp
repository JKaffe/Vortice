/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "vv_wrappers.hpp"

namespace vortice {
	class wGlfwWindow;
}

namespace vvWrappers {
	class VvInstance;

	class VvSurface : public VvWrapper<VkSurfaceKHR> {
	private:
		VvInstance& instance;

		bool m_phys_dev_bound = false, m_size_set = false;
		VkExtent2D m_max_size, m_min_size, m_size;
		uint32_t m_img_count;
		VkSurfaceTransformFlagBitsKHR m_transform;
		VkFormat m_format;
		VkColorSpaceKHR m_color_space;
	public:
		VvSurface(VkSurfaceKHR surface, VvInstance& instance) noexcept;

		VkSurfaceCapabilitiesKHR getDeviceCapabilities(VkPhysicalDevice phys_dev);

		std::vector<VkSurfaceFormatKHR> getDeviceFormats(VkPhysicalDevice phys_dev);

		bool supportsFormat(VkPhysicalDevice phys_dev, VkFormat format, VkColorSpaceKHR color_space);

		void bindPhysicalDevice(VkPhysicalDevice phys_dev, VkFormat format, VkColorSpaceKHR color_space);

		void setSize(VkExtent2D size);

		VkExtent2D getSize() const;

		uint32_t getImageCount() const;

		VkSurfaceTransformFlagBitsKHR getTransform() const;

		VkFormat getFormat() const;

		VkColorSpaceKHR getColorSpace() const;
	};

	class VvImageView : public VvWrapper<VkImageView> {
	private:
		VkDevice m_vk_device;
	public:
		VvImageView(VkDevice device, VkImage image, VkImageViewType type, VkFormat format);
		~VvImageView();
	};
}