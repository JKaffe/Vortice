/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#include <cstring>
#include <optional>
#include <stdexcept>
#include <vulkan/vulkan_core.h>

#include "vv_device.hpp"

/*****************************************************************************
	VvInstance.
******************************************************************************/

vvWrappers::VvInstance::VvInstance(const VkApplicationInfo *const app_info, const std::vector<std::string>& layers, const std::vector<std::string>& extensions) {
	VkResult result;

	uint32_t vk_version;
	result = vkEnumerateInstanceVersion(&vk_version);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not get Vulkan API version.");

	printf("System supports up to Vulkan: %d.%d.\n", VK_API_VERSION_MAJOR(vk_version), VK_API_VERSION_MINOR(vk_version));

	if (vk_version < app_info->apiVersion)
		throw std::runtime_error("Vulkan API version lower than required.");

	std::vector<const char *> layer_names = VvGeneral::unpack(layers);
	std::vector<const char *> extension_names = VvGeneral::unpack(extensions);

	extension_names.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

	VkDebugUtilsMessengerCreateInfoEXT debug_create_info = {
		.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
		.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
							VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
							VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
							VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
		.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
						VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT,
		.pfnUserCallback =
			[](VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
				VkDebugUtilsMessageTypeFlagsEXT messageTypes,
				const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
				void* pUserData) {
					printf("Message ID Number: %d\n", pCallbackData->messageIdNumber);
					if (pCallbackData->pMessageIdName)
						printf("Message ID Name: %s\n", pCallbackData->pMessageIdName);

					printf("Message: %s\n", pCallbackData->pMessage);

					return VK_FALSE;
			},
	};

	VkInstanceCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pNext = &debug_create_info,
		.pApplicationInfo = app_info,
		.enabledLayerCount = (uint32_t)layer_names.size(),
		.ppEnabledLayerNames = layer_names.data(),
		.enabledExtensionCount = (uint32_t)extension_names.size(),
		.ppEnabledExtensionNames = extension_names.data(),
	};

	result = vkCreateInstance(&create_info, nullptr, &m_vk_type);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not create instance.");

	PFN_vkCreateDebugUtilsMessengerEXT pfnCreateDebugUtilsMessengerEXT = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(m_vk_type, "vkCreateDebugUtilsMessengerEXT");
	result = pfnCreateDebugUtilsMessengerEXT(m_vk_type, &debug_create_info, nullptr, &m_vk_debug_utils);
	if (result != VK_SUCCESS) {
		vkDestroyInstance(m_vk_type, nullptr);
		throw std::runtime_error("Could not creaate debug utils messanger.");
	}
}

vvWrappers::VvInstance::~VvInstance() noexcept {
	PFN_vkDestroyDebugUtilsMessengerEXT pfnDestroyDebugUtilsMessengerEXT = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(m_vk_type, "vkDestroyDebugUtilsMessengerEXT");
	pfnDestroyDebugUtilsMessengerEXT(m_vk_type, m_vk_debug_utils, nullptr);
	vkDestroyInstance(m_vk_type, nullptr);
}

std::vector<VkLayerProperties> vvWrappers::VvInstance::getAllLayerProperties() {
	uint32_t property_count;
	vkEnumerateInstanceLayerProperties(&property_count, nullptr);

	std::vector<VkLayerProperties> layer_properies(property_count);
	vkEnumerateInstanceLayerProperties(&property_count, layer_properies.data());

	return layer_properies;
}

std::vector<std::string> vvWrappers::VvInstance::supportsLayers(std::vector<std::string>& requested_layers) {
	std::vector<VkLayerProperties> supported_properties = getAllLayerProperties();

	std::vector<std::string> supported_layers;

	for (auto& property : supported_properties) {
		for (auto& name : requested_layers) {
			if (std::strncmp(name.c_str(), property.layerName, sizeof(property.layerName)) == 0) {
				supported_layers.push_back(name);
				break;
			}
		}
	}

	return supported_layers;
}

/*****************************************************************************
	VvPhysicalDevice.
******************************************************************************/

std::vector<VkPhysicalDevice> vvWrappers::VvPhysicalDevice::getDevices(VvInstance& instance) {
	VkResult result;

	uint32_t dev_count;
	result = vkEnumeratePhysicalDevices(instance.unwrap(), &dev_count, nullptr);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not get the count of physical devices.");

	std::vector<VkPhysicalDevice> phys_devs(dev_count);
	result = vkEnumeratePhysicalDevices(instance.unwrap(), &dev_count, phys_devs.data());
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not get physical devices.");

	return phys_devs;
}

bool vvWrappers::VvPhysicalDevice::hasExtensions(VkPhysicalDevice phys_dev, const std::vector<std::string>& extensions) {
	uint32_t property_count;
	VkResult result = vkEnumerateDeviceExtensionProperties(phys_dev, nullptr, &property_count, nullptr);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not get device extensions.");

	std::vector<VkExtensionProperties> properties(property_count);
	result = vkEnumerateDeviceExtensionProperties(phys_dev, nullptr, &property_count, properties.data());
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not get device extensions.");

	uint32_t extensions_present = 0;
	for (auto& ext : extensions) {
		for (auto& property : properties) {
			if (std::strncmp(ext.c_str(), property.extensionName, sizeof(property.extensionName)) == 0) {
				extensions_present++;
				break;
			}
		}
	}

	return extensions_present == extensions.size();
}

std::optional<uint32_t> vvWrappers::VvPhysicalDevice::getQueueFamilyIdx(VkPhysicalDevice phys_dev, VkQueueFlags queue_cap) {
	uint32_t family_count;
	vkGetPhysicalDeviceQueueFamilyProperties(phys_dev, &family_count, NULL);

	std::vector<VkQueueFamilyProperties> properties_vec(family_count);
	vkGetPhysicalDeviceQueueFamilyProperties(phys_dev, &family_count, properties_vec.data());

	for (uint32_t i { 0 } ; auto properties : properties_vec) {
		if (properties.queueFlags & queue_cap)
			return i;

		i++;
	}

	return std::nullopt;
}

std::optional<uint32_t> vvWrappers::VvPhysicalDevice::getMemoyHeapIdx(VkPhysicalDevice phys_dev, VkMemoryPropertyFlags heap_cap, uint32_t size) {
	VkPhysicalDeviceMemoryProperties mem_properties;
	vkGetPhysicalDeviceMemoryProperties(phys_dev, &mem_properties);

	// This assumes that the heap's size cannot be 0. It it is allowed we might be returning the index to a heap with 0 size.
	for (uint32_t i = 0; i < mem_properties.memoryTypeCount; i++)
		if (mem_properties.memoryTypes[i].propertyFlags & heap_cap)
			if (mem_properties.memoryHeaps[mem_properties.memoryTypes[i].heapIndex].size >= size)
				return i;

	return std::nullopt;
}

bool vvWrappers::VvPhysicalDevice::supportsPresentation(VkPhysicalDevice device, uint32_t queue_family_idx, VkSurfaceKHR surface) {
	VkBool32 has_support = false;
	VkResult result = vkGetPhysicalDeviceSurfaceSupportKHR(device, queue_family_idx, surface, &has_support);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not determine if queue family has presentation support.");

	return VvGeneral::unpack(has_support);
}

/*****************************************************************************
	VvDevice.
******************************************************************************/

vvWrappers::VvDevice::VvDevice(VkPhysicalDevice phys_dev, uint32_t queue_family_idx, const std::vector<std::string>& extensions) : m_queue_family_idx(queue_family_idx){
	const float queue_priority = 1.0f;
	const VkDeviceQueueCreateInfo queue_create_info = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
		.queueFamilyIndex = queue_family_idx,
		.queueCount = 1,
		.pQueuePriorities = &queue_priority,
	};

	const VkPhysicalDeviceFeatures phys_dev_features = { 0 };

	std::vector<const char *> extension_names = VvGeneral::unpack(extensions);

	const VkDeviceCreateInfo dev_create_info = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.queueCreateInfoCount = 1,
		.pQueueCreateInfos = &queue_create_info,
		.enabledExtensionCount = (uint32_t)extension_names.size(),
		.ppEnabledExtensionNames = extension_names.data(),
		.pEnabledFeatures = &phys_dev_features,
	};

	VkResult result = vkCreateDevice(phys_dev, &dev_create_info, nullptr, &m_vk_type);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not create VkDevice.");
}

vvWrappers::VvDevice::~VvDevice() {
	vkDestroyDevice(m_vk_type, nullptr);
}

VkQueue vvWrappers::VvDevice::getQueue() {
	VkQueue queue;
	vkGetDeviceQueue(unwrap(), m_queue_family_idx, 0, &queue);

	return queue;
}

/*****************************************************************************
	VvDeviceMemory.
******************************************************************************/

vvWrappers::VvDeviceMemory::VvDeviceMemory(VvDevice& device, uint32_t heap_idx, uint64_t size) : m_vk_device(device.unwrap()) {
	const VkMemoryAllocateInfo alloc_info = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		.allocationSize = size,
		.memoryTypeIndex = heap_idx,
	};

	VkResult result = vkAllocateMemory(m_vk_device, &alloc_info, nullptr, &m_vk_type);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not allocate device memory.");
}

vvWrappers::VvDeviceMemory::~VvDeviceMemory() {
	vkFreeMemory(m_vk_device, m_vk_type, nullptr);
}