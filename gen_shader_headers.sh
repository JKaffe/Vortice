#!/bin/bash

# Copyright (C) 2022 Karmjit Mahil.
#
# This file is part of Vortice.
#
# Vortice is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Vortice. If not, see <https://www.gnu.org/licenses/>.

GLSLC_ARGS="-Werror --target-env=vulkan1.0 -O -mfmt=c -o /dev/stdout"
SHADER_SRC="$1"
OUT_DIR="$3"

[ "$2" = "-v" ] && GLSLC_ARGS="-fshader-stage=vert $GLSLC_ARGS" && SHADER_NAME="vertex_spv"
[ "$2" = "-f" ] && GLSLC_ARGS="-fshader-stage=frag $GLSLC_ARGS" && SHADER_NAME="fragment_spv"
[ "$2" = "-c" ] && GLSLC_ARGS="-fshader-stage=comp $GLSLC_ARGS" && SHADER_NAME="compute_spv"

SHADER=$(glslc $GLSLC_ARGS "$SHADER_SRC")
[ $? != 0 ] && exit 1

cat > "$OUT_DIR" << EOF
#pragma once

// Generated using $(basename $0)
// Source file:    $SHADER_SRC

#include <cstdint>

namespace vortice { namespace shader {

static const uint32_t $SHADER_NAME[] =
$SHADER;

}}
EOF