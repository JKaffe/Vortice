/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "vv_wrappers.hpp"

namespace vvWrappers {
	class VvShaderModule : public VvWrapper<VkShaderModule> {
	private:
		VkDevice m_vk_device;
		VkShaderStageFlagBits stage;
	public:
		VvShaderModule(VkDevice device, VkShaderStageFlagBits stage, const uint32_t *const spv, uint32_t size);
		~VvShaderModule();

		const VkPipelineShaderStageCreateInfo getPipelineCreateInfoSubStruct() const noexcept;
	};

	class VvPipelineLayout : public VvWrapper<VkPipelineLayout> {
	private:
		VkDevice m_vk_device;
	public:
		VvPipelineLayout(VkDevice device, const VkPushConstantRange *push_constant_range = nullptr, const VkDescriptorSetLayout *desc_set_layout = nullptr);
		~VvPipelineLayout();
	};
}