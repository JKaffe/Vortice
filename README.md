# Vortice
Vulkan demo inspired by the [Mathologer Vortex Math video](https://youtu.be/6ZrO90AI0c8).


A compute pipeline will be used to calculate the vertices for the chords and a graphics pipeline to display them.

## How to build and run.

```bash
meson builddir
ninja -C builddir
./builddir/vortice
```

## Samples.
### Not corresponding to the video.
<img src="pattern-0.png" width="300">
<img src="pattern-5.png" width="300">
<img src="pattern-1.png" width="300">
<img src="pattern-2.png" width="300">
<img src="pattern-3.png" width="300">
<img src="pattern-4.png" width="300">

# Licensing
The project is licensed under GPL3+.