/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "vv_wrappers.hpp"

namespace vvWrappers {
	class VvDevice;
	class VvImageView;
	class VvSurface;
	class VvDeviceMemory;
	class VvShaderModule;
	class VvPipelineLayout;

	class VvSwapchain : public VvWrapper<VkSwapchainKHR> {
	private:
		VkDevice m_vk_device;
		VkFormat m_format;

		std::vector<VkImage> m_images;
		std::vector<VvImageView *> m_image_views;
	public:
		VvSwapchain(VvDevice& device, VvSurface& surface);
		~VvSwapchain();

		const std::vector<VvImageView *>& getImageViews();
	};

	class VvRenderPass : public VvWrapper<VkRenderPass> {
	private:
		VkDevice m_vk_device;
	public:
		VvRenderPass(const VvSurface& surface, const VvDevice& device);
		~VvRenderPass();
	};

	class VvGraphicsPipeline : public VvWrapper<VkPipeline> {
	private:
		VkDevice m_vk_device;
	public:
		VvGraphicsPipeline(const VvDevice& device,
						   const VvShaderModule& vert_module,
						   const VvShaderModule& frag_module,
						   const VvSurface& surface,
						   const VvPipelineLayout& pipeline_layout,
						   const VvRenderPass& render_pass,
						   const VkVertexInputBindingDescription *vertex_binding_desc = nullptr,
						   const VkVertexInputAttributeDescription *vertex_attribute_desc = nullptr);
		~VvGraphicsPipeline();
	};

	class VvFramebuffer : public VvWrapper<VkFramebuffer> {
	private:
		VkDevice m_vk_device;
		VkRenderPass m_vk_render_pass;
	public:
		VvFramebuffer(VvDevice& device, VvRenderPass& render_pass, VvImageView& img_view, VkExtent2D size);
		~VvFramebuffer();

		VkRenderPass getRenderPass() const {
			return m_vk_render_pass;
		}
	};

	class VvCommandBuffer : public VvWrapper<VkCommandBuffer> {
	private:
		VkDevice m_vk_device;
		VkCommandPool m_vk_cmd_pool;

		enum { INIT, RECORDING, END} m_state;
	public:
		VvCommandBuffer(VvDevice& device, uint32_t queue_family_idx);
		~VvCommandBuffer();

		void beginRecording();
		void endRecording();

		void beginRenderPass(VvFramebuffer& framebuffer, VkExtent2D size);
		void endRenderPass();

		void reset();

		void bindPipeline(VvGraphicsPipeline& pipeline);

		void draw(uint32_t vertex_count);
	};
}