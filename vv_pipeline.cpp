/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdexcept>
#include <vulkan/vulkan_core.h>

#include "vv_pipeline.hpp"

/*****************************************************************************
	VvShaderModule.
******************************************************************************/

vvWrappers::VvShaderModule::VvShaderModule(VkDevice device, VkShaderStageFlagBits stage, const uint32_t *const spv, uint32_t size) : m_vk_device(device), stage(stage) {
	const VkShaderModuleCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
		.codeSize = size,
		.pCode = spv
	};

	VkResult result = vkCreateShaderModule(m_vk_device, &create_info, nullptr, &m_vk_type);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not create shader module.");
}

vvWrappers::VvShaderModule::~VvShaderModule() {
	vkDestroyShaderModule(m_vk_device, m_vk_type, nullptr);
}

const VkPipelineShaderStageCreateInfo vvWrappers::VvShaderModule::getPipelineCreateInfoSubStruct() const noexcept {
	VkPipelineShaderStageCreateInfo info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
		.stage = stage,
		.module = m_vk_type,
		.pName = "main",
	};

	return info;
}

/*****************************************************************************
	VvPipelineLayout.
******************************************************************************/

vvWrappers::VvPipelineLayout::VvPipelineLayout(VkDevice device, const VkPushConstantRange *push_constant_range, const VkDescriptorSetLayout *desc_set_layout) : m_vk_device(device) {
	const VkPipelineLayoutCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
		.setLayoutCount = desc_set_layout != nullptr,
		.pSetLayouts = desc_set_layout,
		.pushConstantRangeCount = push_constant_range != nullptr,
		.pPushConstantRanges = push_constant_range,
	};

	VkResult result = vkCreatePipelineLayout(m_vk_device, &create_info, nullptr, &m_vk_type);
	if (result != VK_SUCCESS)
		throw std::runtime_error("Could not create pipeline layout.");
}

vvWrappers::VvPipelineLayout::~VvPipelineLayout() {
	vkDestroyPipelineLayout(m_vk_device, m_vk_type, nullptr);
}