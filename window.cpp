/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */


#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <stdexcept>
#include <vulkan/vulkan_core.h>

#include "vortice.hpp"
#include "vv_wrappers.hpp"

/*****************************************************************************
	Window private.
******************************************************************************/

vortice::wGlfwWindow::wGlfwWindow(const char *const title, const VkExtent2D window_size) {
	uint32_t ret = glfwInit();
	if (ret != GLFW_TRUE)
		throw std::runtime_error("Could not initialise GLFW library.");

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	m_window = glfwCreateWindow(window_size.width, window_size.width, title, nullptr, nullptr);
	if (m_window == NULL)
		throw std::runtime_error("Could not create window.");
}

/*****************************************************************************
	Window public.
******************************************************************************/

vortice::wGlfwWindow::~wGlfwWindow() {
	glfwDestroyWindow(m_window);
	glfwTerminate();
}

// Let's not worry about thread saftey.
vortice::wGlfwWindow *vortice::wGlfwWindow::getInstance(const char *const title, const VkExtent2D window_size) {


	if (instance == nullptr) {
		instance = new wGlfwWindow(title, window_size);
	}

	return instance;
}

bool vortice::wGlfwWindow::shouldClose() {
	return glfwWindowShouldClose(m_window);
};

void vortice::wGlfwWindow::pollEvents() {
	glfwPollEvents();
}

void vortice::wGlfwWindow::setKeyCallback(vortice::Demo *demo, key_callback_fun callback) {
	m_key_callback_state = {
		.demo = demo,
		.callback_fun = callback,
	};

	glfwSetWindowUserPointer(m_window, &m_key_callback_state);

	glfwSetKeyCallback(m_window, [](GLFWwindow* window, int key, int scancode, int action, int mods){
        struct key_callback_state *callback_state = (struct key_callback_state*)glfwGetWindowUserPointer(window);
		callback_state->callback_fun(*callback_state->demo, key, scancode, action, mods);
	});
}

VkSurfaceKHR vortice::wGlfwWindow::acquireSurface(VkInstance instance){
	if (!m_vk_surface.has_value()) {
		VkSurfaceKHR surface;
		VkResult result = glfwCreateWindowSurface(instance, m_window, nullptr, &surface);
		if (result != VK_SUCCESS)
			throw std::runtime_error("Could not create window surface.");

		m_vk_surface = surface;
	}

	return m_vk_surface.value();
}

void vortice::wGlfwWindow::releaseSurface(VkInstance instance) {
	vkDestroySurfaceKHR(instance, m_vk_surface.value(), nullptr);
}

VkExtent2D vortice::wGlfwWindow::getSize() noexcept {
	int width, height;
	glfwGetFramebufferSize(m_window, &width, &height);

	return { .width = (uint32_t)width, .height = (uint32_t)height };
}

std::vector<std::string> vortice::wGlfwWindow::getRequiredExtensions() {
	uint32_t ext_count;
	const char **extensions = glfwGetRequiredInstanceExtensions(&ext_count);
	if (extensions == NULL)
		throw std::runtime_error("Some GLFW required extentions are not avialble.");

	return vvWrappers::VvGeneral::pack(extensions, ext_count);
}

vortice::wGlfwWindow *vortice::wGlfwWindow::instance = nullptr;