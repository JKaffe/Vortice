/* Copyright (C) 2022 Karmjit Mahil.
 *
 * This file is part of Vortice.
 *
 * Vortice is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Vortice is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Vortice. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <optional>
#include <stdexcept>
#include <vulkan/vulkan_core.h>

#include "vv_wrappers.hpp"

namespace vvWrappers {
	static const std::string KHRONOS_VALIDATION_LAYER_NAME = "VK_LAYER_KHRONOS_validation";

    class VvInstance : public VvWrapper<VkInstance> {
    private:
        VkDebugUtilsMessengerEXT m_vk_debug_utils;

    public:
        VvInstance(const VkApplicationInfo *const app_info, const std::vector<std::string>& layers, const std::vector<std::string>& extensions);
        ~VvInstance() noexcept;

        static std::vector<VkLayerProperties> getAllLayerProperties();
        static std::vector<std::string> supportsLayers(std::vector<std::string>& requested_layers);
    };

    class VvPhysicalDevice : public VvWrapper<VkPhysicalDevice> {
    public:
        static std::vector<VkPhysicalDevice> getDevices(VvInstance& instance);
        static bool hasExtensions(VkPhysicalDevice phys_dev, const std::vector<std::string>& extensions);

        static std::optional<uint32_t> getQueueFamilyIdx(VkPhysicalDevice phys_dev, VkQueueFlags queue_cap);
        static std::optional<uint32_t> getMemoyHeapIdx(VkPhysicalDevice phys_dev, VkMemoryPropertyFlags heap_cap, uint32_t size = 0);

        static bool supportsPresentation(VkPhysicalDevice device, uint32_t queue_family_idx, VkSurfaceKHR surface);

        static const char *toString(VkPhysicalDeviceType dev_type) noexcept {
            #define TYPE(dev_type, str)                                \
                case VK_PHYSICAL_DEVICE_TYPE_##dev_type: {             \
                    static constexpr const char *const dev_type = str; \
                    return dev_type;                                   \
                }

            switch (dev_type) {
                TYPE(OTHER, "Other")
                TYPE(INTEGRATED_GPU, "iGPU")
                TYPE(DISCRETE_GPU, "GPU")
                TYPE(VIRTUAL_GPU, "vGPU")
                TYPE(CPU, "CPU")

                default:
                    __builtin_unreachable();
            }

            #undef TYPE
        }

        static void printNames(std::vector<VkPhysicalDevice>& phys_devs) noexcept {
            VkPhysicalDeviceProperties properties;

            printf("Physical Devices:\n");
            for (uint32_t i = 0; i < phys_devs.size(); i++) {
                vkGetPhysicalDeviceProperties(phys_devs[i], &properties);
                printf("[%u]:\t%s (%s).\n", i, properties.deviceName, toString(properties.deviceType));
            }
        }
    };

    class VvDevice : public VvWrapper<VkDevice> {
    private:
        uint32_t m_queue_family_idx;

    public:
        VvDevice(VkPhysicalDevice phys_dev, uint32_t queue_family_idx, const std::vector<std::string>& extensions);
        ~VvDevice();

        VkQueue getQueue();
    };

    class VvDeviceMemory : public VvWrapper<VkDeviceMemory> {
    private:
        VkDevice m_vk_device;

    public:
        VvDeviceMemory(VvDevice& device, uint32_t heap_idx, uint64_t size);
        ~VvDeviceMemory();
    };
}